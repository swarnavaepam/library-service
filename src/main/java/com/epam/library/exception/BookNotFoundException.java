package com.epam.library.exception;

public class BookNotFoundException extends RuntimeException {

	
	private static final long serialVersionUID = 7387671330119106297L;

	public BookNotFoundException(String message) {
		super(message);
	}


}
