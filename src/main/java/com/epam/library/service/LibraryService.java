package com.epam.library.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.epam.library.model.BookDto;
import com.epam.library.model.LibraryDto;
import com.epam.library.model.UserDto;

public interface LibraryService {
	ResponseEntity<List<BookDto>> getBooks();
	ResponseEntity<?> getBookById(int id);
	ResponseEntity<?> addNewBook(BookDto bookDto);	
	ResponseEntity<?> updateBook(BookDto bookDto,int id);		
	ResponseEntity<?> deleteBookById(int id);
	ResponseEntity<?> fetchAllUser();
	ResponseEntity<?> fetchUser(Long id);
	ResponseEntity<?> addUser( UserDto userDto);
	ResponseEntity<?> update(UserDto userDto, Long id);	
	ResponseEntity<?> delete(Long id);
	LibraryDto addAssociation(LibraryDto library);
	void deleteAssociation(LibraryDto library);
	
}
