package com.epam.library.fallbacks;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.epam.library.client.UserServiceClient;
import com.epam.library.model.UserDto;

@Component
public class UserServiceFallBack implements UserServiceClient {

	@Override
	public ResponseEntity<?> fetchAllUser() {
		return ResponseEntity.ok("Service unavailable");
	}

	@Override
	public ResponseEntity<UserDto> fetchUser(Long id) {
		return ResponseEntity.ok(new UserDto());
	}

	@Override
	public ResponseEntity<?> addUser(UserDto userDto) {
		return ResponseEntity.ok("Service unavailable");
	}

	@Override
	public ResponseEntity<?> update(UserDto userDto, Long id) {
		return ResponseEntity.ok("Service unavailable");
	}

	@Override
	public ResponseEntity<?> delete(Long id) {
		return ResponseEntity.ok("Service unavailable");
	}

}
