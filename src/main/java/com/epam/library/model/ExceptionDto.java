package com.epam.library.model;

import java.time.LocalDateTime;

public class ExceptionDto {
	private String message;
	private LocalDateTime timeStamp;

	public ExceptionDto(String message, LocalDateTime date) {
		super();
		this.message = message;
		this.timeStamp = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getDate() {
		return timeStamp;
	}

	public void setDate(LocalDateTime date) {
		this.timeStamp = date;
	}
	
}
