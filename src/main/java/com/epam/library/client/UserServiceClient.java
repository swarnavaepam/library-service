package com.epam.library.client;

import java.net.URI;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.library.fallbacks.UserServiceFallBack;
import com.epam.library.model.UserDto;

@FeignClient(name = "user-service-app",fallback = UserServiceFallBack.class,decode404 = true)
public interface UserServiceClient {

	@GetMapping("/user")
	public ResponseEntity<?> fetchAllUser();
		
	@GetMapping("/user/{id}")
	public ResponseEntity<UserDto> fetchUser(@PathVariable Long id);
		
	@PostMapping
	public ResponseEntity<?> addUser(@RequestBody UserDto userDto);
	
	@PutMapping("/user/{id}")
	public ResponseEntity<?> update(@RequestBody UserDto userDto, @PathVariable Long id);
		
	@DeleteMapping("/user/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id);
}
