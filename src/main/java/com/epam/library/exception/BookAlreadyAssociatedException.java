package com.epam.library.exception;

public class BookAlreadyAssociatedException extends RuntimeException {

	private static final long serialVersionUID = -2369597482919009335L;

	public BookAlreadyAssociatedException(String message) {
		super(message);
	}

}
