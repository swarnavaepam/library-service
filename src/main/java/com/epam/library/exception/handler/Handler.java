package com.epam.library.exception.handler;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.epam.library.exception.BookAlreadyAssociatedException;
import com.epam.library.exception.BookNotFoundException;
import com.epam.library.exception.NoLibraryAssociationFoundException;
import com.epam.library.exception.UserNotFoundException;
import com.epam.library.model.ExceptionDto;


@RestControllerAdvice
public class Handler {
	
	@ExceptionHandler(value = NoLibraryAssociationFoundException.class)
	public ResponseEntity<?> handleLibraryException(HttpServletRequest request,NoLibraryAssociationFoundException exception) {
		ExceptionDto exceptionDto = new ExceptionDto("No such Association Found!", LocalDateTime.now());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDto);
	}
	@ExceptionHandler(value = BookNotFoundException.class)
	public ResponseEntity<?> handleLibraryException(HttpServletRequest request,BookNotFoundException exception) {
		ExceptionDto exceptionDto = new ExceptionDto("No such Book Found!", LocalDateTime.now());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDto);
	}
	@ExceptionHandler(value =BookAlreadyAssociatedException.class)
	public ResponseEntity<?> handleLibraryException(HttpServletRequest request,BookAlreadyAssociatedException exception) {
		ExceptionDto exceptionDto = new ExceptionDto("The book is currently not available as it is already associated with someonelse!", LocalDateTime.now());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDto);
	}
	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<?> handleLibraryException(HttpServletRequest request,UserNotFoundException exception) {
		ExceptionDto exceptionDto = new ExceptionDto("No such User Found!", LocalDateTime.now());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exceptionDto);
	}
}
