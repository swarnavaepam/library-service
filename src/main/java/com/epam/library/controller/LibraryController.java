package com.epam.library.controller;

import java.net.URI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.library.model.BookDto;
import com.epam.library.model.LibraryDto;
import com.epam.library.model.UserDto;
import com.epam.library.service.LibraryService;

@RestController
@RequestMapping("/library")
public class LibraryController {
	
	@Autowired
	LibraryService libraryService;
	
	private static final Logger log = LogManager.getLogger(LibraryController.class);
	
	//Login
	@GetMapping
	public ResponseEntity<?> get(){
		return ResponseEntity.ok("Yet to implement");
	}
	
	@GetMapping("/books")
	public ResponseEntity<?> getAllBooks(){
		return libraryService.getBooks();
	}
	
	@GetMapping("/books/{id}")
	public ResponseEntity<?> getBook(@PathVariable int id){
		return libraryService.getBookById(id);
	}
	
	//Admin
	@PostMapping
	public ResponseEntity<?> addBook(@RequestBody BookDto bookDto){
		return libraryService.addNewBook(bookDto);
	}
	
	//Admin
	@DeleteMapping("/books/{id}")
	public ResponseEntity<?> deleteBook(@PathVariable int id){
		return libraryService.deleteBookById(id);
	}
	
	@GetMapping("/users")
	public ResponseEntity<?> getAllUsers(){
		return libraryService.fetchAllUser();
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<?> getUser(@PathVariable Long id){
		return libraryService.fetchUser(id);
	}
	
	@PostMapping("/users")
	public ResponseEntity<?> addUser(@RequestBody UserDto userDto){
		return libraryService.addUser(userDto);
	}
	
	@DeleteMapping("/users/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Long id){
		return libraryService.delete(id);
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity<?> updateUser(@RequestBody UserDto userDto,@PathVariable Long id){
		return libraryService.update(userDto, id);
	}
	
	@PostMapping("/users/{user_id}/books/{book_id}")
	public ResponseEntity<?> addLibraryAssociation(@PathVariable("user_id") Long userId,
												   @PathVariable("book_id") int bookId){
		log.info("Creating new user association in library");
		LibraryDto savedLibraryUser = libraryService.addAssociation(new LibraryDto(userId,bookId));
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
	            .path("/{id}")
	            .buildAndExpand(savedLibraryUser.getId())
	            .toUri();

		return ResponseEntity.created(location).body(savedLibraryUser);
	}
	
	@DeleteMapping("/users/{user_id}/books/{book_id}")
	public ResponseEntity<?> removeLibraryAssociation(@PathVariable("user_id") Long userId,
													  @PathVariable("book_id") int bookId){
		
		libraryService.deleteAssociation(new LibraryDto(userId,bookId));
		return ResponseEntity.noContent().build();
	}
}





