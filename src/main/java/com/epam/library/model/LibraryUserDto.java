package com.epam.library.model;

import java.util.List;

public class LibraryUserDto {
	protected UserDto user;
	protected List<BookDto> books;
	public LibraryUserDto(UserDto user, List<BookDto> books) {
		super();
		this.user = user;
		this.books = books;
	}
	public UserDto getUser() {
		return user;
	}
	public void setUser(UserDto user) {
		this.user = user;
	}
	public List<BookDto> getBooks() {
		return books;
	}
	public void setBooks(List<BookDto> books) {
		this.books = books;
	}
	
}
