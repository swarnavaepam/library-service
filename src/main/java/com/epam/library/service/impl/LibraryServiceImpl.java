package com.epam.library.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.epam.library.client.BookServiceClient;
import com.epam.library.client.UserServiceClient;
import com.epam.library.entity.Library;
import com.epam.library.exception.BookAlreadyAssociatedException;
import com.epam.library.exception.BookNotFoundException;
import com.epam.library.exception.NoLibraryAssociationFoundException;
import com.epam.library.exception.UserNotFoundException;
import com.epam.library.model.BookDto;
import com.epam.library.model.LibraryDto;
import com.epam.library.model.LibraryUserDto;
import com.epam.library.model.UserDto;
import com.epam.library.repository.LibraryRepository;
import com.epam.library.service.LibraryService;
import com.epam.library.utils.Converter;

@Service
public class LibraryServiceImpl implements LibraryService {
	@Autowired
	UserServiceClient userService;
	
	@Autowired
	BookServiceClient bookService;
	
	@Autowired
	LibraryRepository libraryRepository;
	
	@Autowired
	Converter converter;
	
	@Override
	public ResponseEntity<List<BookDto>> getBooks() {
		return  bookService.getBooks();
	}

	@Override
	public ResponseEntity<?> getBookById(int id){
		System.out.println("--------------"+bookService.getBookById(id).getBody());
		return bookService.getBookById(id);
	}

	@Override
	public ResponseEntity<?> addNewBook(BookDto bookDto) {
		return bookService.addNewBook(bookDto);
	}

	@Override
	public ResponseEntity<?> updateBook(BookDto bookDto, int id) {
		return bookService.updateBook(bookDto, id);
	}

	@Override
	public ResponseEntity<?> deleteBookById(int id) {
		Optional<Library> library = libraryRepository.findByBookId(id);
		library.ifPresent(lib->libraryRepository.delete(lib));
		return bookService.deleteBookById(id);
	}

	@Override
	public ResponseEntity<?> fetchAllUser() {
		return userService.fetchAllUser();
	}

	@Override
	public ResponseEntity<?> fetchUser(Long id) {
		ResponseEntity<?> response = userService.fetchUser(id);
		if(response.getStatusCodeValue() == 404) {
			throw new UserNotFoundException("No such user is available");
		}
		List<BookDto> books = new ArrayList<>();
		for(Library library:libraryRepository.findAllByUserId(id)) {
				books.add((BookDto)bookService.getBookById(library.getBookId()).getBody());
		}
		return ResponseEntity.ok(new LibraryUserDto((UserDto)response.getBody(), books));
	}

	@Override 
	public ResponseEntity<?> addUser(UserDto userDto) {
		return userService.addUser(userDto);
	}

	@Override
	public ResponseEntity<?> update(UserDto userDto, Long id) {
		return userService.update(userDto, id);
	}

	@Override
	public ResponseEntity<?> delete(Long id) {
		if(!libraryRepository.findAllByUserId(id).isEmpty()) {
			libraryRepository.deleteAll(libraryRepository.findAllByUserId(id));
		}
		return userService.delete(id);
	}

	@Override
	public LibraryDto addAssociation(LibraryDto libraryDto) {
		if(libraryRepository.findByBookId(libraryDto.getBookId()).isPresent()){
			 throw new BookAlreadyAssociatedException("Book is not available");
		}
		if(bookService.getBookById(libraryDto.getBookId()).getStatusCodeValue()==404) {
			throw new BookNotFoundException("No such book is available");
		}
		if(userService.fetchUser(libraryDto.getUserId()).getStatusCodeValue()==404) {
			throw new UserNotFoundException("No such user is available");
		}
		return converter.covertEntityToDto(libraryRepository.save(converter.covertDtoToEntity(libraryDto)));
	}

	@Override
	public void deleteAssociation(LibraryDto libraryDto) {
		Library library = libraryRepository.findByBookId(libraryDto.getBookId())
						 .orElseThrow(() -> new NoLibraryAssociationFoundException("no association found"));
		libraryRepository.deleteByBookId(library.getBookId());
	}

}
