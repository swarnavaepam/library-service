package com.epam.library.model;

import javax.validation.constraints.Size;

public class BookDto {
	private int id;
	@Size(min = 3, max = 10)
	private String name;
	private String description;
	private String category;
	private AuthorDto author;
	public BookDto() {
		
	}
	public BookDto(int id, String name, String description, String category, AuthorDto author) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.category = category;
		this.author = author;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public AuthorDto getAuthor() {
		return author;
	}
	public void setAuthor(AuthorDto author) {
		this.author = author;
	}
}
