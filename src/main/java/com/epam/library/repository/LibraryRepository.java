package com.epam.library.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;

import com.epam.library.entity.Library;

public interface LibraryRepository extends CrudRepository<Library, Long>{
	Optional<Library> findByBookId(int bookId);
	List<Library> findAllByUserId(Long userId);
	@Transactional
	@Modifying
	void deleteByBookId(int bookId);
}
