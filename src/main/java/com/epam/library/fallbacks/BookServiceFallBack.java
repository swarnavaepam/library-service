package com.epam.library.fallbacks;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.epam.library.client.BookServiceClient;
import com.epam.library.model.BookDto;

@Component
public class BookServiceFallBack implements BookServiceClient {

	@Override
	public ResponseEntity<List<BookDto>> getBooks() {
		return ResponseEntity.ok(new ArrayList<BookDto>());
	}

	@Override
	public ResponseEntity<BookDto> getBookById(int id) {
		return ResponseEntity.ok(new BookDto());
	}

	@Override
	public ResponseEntity<?> addNewBook(@Valid BookDto bookDto) {
		return ResponseEntity.ok("This is a fallback");
	}

	@Override
	public ResponseEntity<?> updateBook(@Valid BookDto bookDto, int id) {
		return ResponseEntity.ok("This is a fallback");
	}

	@Override
	public ResponseEntity<?> deleteBookById(int id) {
		return ResponseEntity.ok("This is a fallback");
	}

}
