package com.epam.library.model;

public class LibraryDto {
	
	protected Long id;
	protected Long userId;
	protected int bookId;
	public LibraryDto() {
		
	}
	
	public LibraryDto(Long userId, int bookId) {
		super();
		this.userId = userId;
		this.bookId = bookId;
	}
	
	public Long getId() {
		return id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
}
