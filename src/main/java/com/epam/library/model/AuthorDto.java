package com.epam.library.model;

public class AuthorDto {
	int id;
	String name;
	public AuthorDto() {
		
	}
	public AuthorDto(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
