package com.epam.library.utils;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.library.entity.Library;
import com.epam.library.model.LibraryDto;

@Component
public class Converter {
	@Autowired
	ModelMapper modelMapper;
	
	public Library covertDtoToEntity(LibraryDto libraryDto) {
		return modelMapper.map(libraryDto,Library.class);
	}
	public LibraryDto covertEntityToDto(Library library) {
		return modelMapper.map(library,LibraryDto.class);
	}
}
