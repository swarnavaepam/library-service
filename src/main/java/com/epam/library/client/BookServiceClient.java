package com.epam.library.client;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.library.fallbacks.BookServiceFallBack;
import com.epam.library.model.BookDto;

@FeignClient(name = "book-service-app",fallback =  BookServiceFallBack.class, decode404 = true)
public interface BookServiceClient {
	
	@GetMapping("/books")
	public ResponseEntity<List<BookDto>> getBooks();
	
	@GetMapping("books/{id}")	
	public ResponseEntity<BookDto> getBookById(@PathVariable("id")int id);

	@PostMapping("/books")
	public ResponseEntity<?> addNewBook(@RequestBody @Valid BookDto bookDto);
		
	@PutMapping("books/{id}")
	public ResponseEntity<?> updateBook(@RequestBody @Valid BookDto bookDto,@PathVariable("id")int id);
		
	@DeleteMapping("books/{id}")
	public ResponseEntity<?> deleteBookById(@PathVariable("id")int id);
}
