package com.epam.library.exception;

public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 9206808624082446277L;

	public UserNotFoundException(String message) {
		super("No such user is found");
	}
	
}
