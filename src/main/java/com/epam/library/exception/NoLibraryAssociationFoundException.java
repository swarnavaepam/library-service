package com.epam.library.exception;

public class NoLibraryAssociationFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoLibraryAssociationFoundException(String message) {
		super(message);
	}

}
